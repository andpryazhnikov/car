using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    public GameObject firstCar;
    public GameObject secondCar;
    public GameObject thirdCar;
    public GameObject group;
    public Vector3 mousepos;
    public bool flag = false;
    public int proshl;
    public int i;
    public Animation anim;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0)){
            mousepos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            for (i = 0; i < group.transform.childCount; i++){
                Vector3 pos = group.transform.GetChild(i).gameObject.transform.position;
                if (mousepos.y <= pos.y + 1.5f && mousepos.y >= pos.y - 1.5f && mousepos.x <= pos.x + 0.5f && mousepos.x >= pos.x - 0.5f){
                    if(flag == true){
                        group.transform.GetChild(proshl).GetComponent<SpriteRenderer>().color = Color.white;
                        group.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().color = Color.red;
                        proshl = i;
                    }
                    else{
                        group.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().color = Color.red;
                        flag=true;
                        proshl = i;
                    }
                }
            }
        }
    }

    public void ButnPressed(){
        Debug.Log("Сработало");
        if(i==0){
            Debug.Log("Ок");
            anim.GetComponent<Animation>();
            anim.Play("Menu Animation");
        }
    }
}
