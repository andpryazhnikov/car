using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarMove : MonoBehaviour
{
    public GameObject car;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A)){
           car.transform.position -= new Vector3 (0.05f,0,0);
        }
        if (Input.GetKey(KeyCode.D)){
            car.transform.position += new Vector3(0.05f,0,0);
        }
        if (Input.GetKey(KeyCode.W)){
            car.transform.position += new Vector3(0,0.05f,0);
            car.transform.rotation = Quaternion.Euler(0,0,0);
        }
        if (Input.GetKey(KeyCode.S)){
            car.transform.position -= new Vector3(0,0.05f,0);
            car.transform.rotation = Quaternion.Euler(0,0,180); 
        }
    }
}
